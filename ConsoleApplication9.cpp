﻿#include <iomanip>
#include <iostream>
#include "windows.h"
#include <ctime>
#include <stdlib.h>

using namespace std;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	const int n = 100;
	int a[n];
	for (int i = 0; i < n; i++)
	{
		a[i] = rand() % n;
		if ((a[i] % 6 == 0) && (a[i] != 0))
		{
			cout << "Число кратне 6: " << a[i] << endl;
		}
	}
	int min = a[0];
	for (int i = 1; i < n; i++)
	{
		if ((a[i] < min) && (a[i] % 6 == 0) && (a[i] != 0))
		{
			min = a[i];
		}

	}
	cout << "Найменше кратне 6 в масиві: " << min << endl;
	system("pause");
	return 0;
}

